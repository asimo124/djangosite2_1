# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-17 20:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plates', '0002_licplate'),
    ]

    operations = [
        migrations.CreateModel(
            name='customers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=75, unique=True)),
                ('username', models.CharField(max_length=75, unique=True)),
                ('password', models.CharField(max_length=50)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=75)),
                ('entry_date', models.DateTimeField(verbose_name='date published')),
                ('entryip', models.CharField(max_length=30)),
            ],
        ),
    ]
