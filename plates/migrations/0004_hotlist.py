# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-17 20:58
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('plates', '0003_customers'),
    ]

    operations = [
        migrations.CreateModel(
            name='hotlist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('plate_number', models.CharField(max_length=30)),
                ('entry_date', models.DateTimeField(verbose_name='date_published')),
                ('entryip', models.CharField(max_length=30)),
                ('customer_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plates.customers')),
            ],
        ),
    ]
