from datetime import datetime, timedelta, time
from django.db import models

# Create your models here.
class Plate(models.Model):
    plate_number = models.CharField(max_length=75)
    latitude = models.DecimalField(max_digits=7, decimal_places=7)
    longitude = models.DecimalField(max_digits=7, decimal_places=7)
    plate_image = models.FileField(upload_to='plate_uploads/plates/')
    overview_image = models.FileField(upload_to='plate_uploads/overviews/')
    entry_date = models.DateTimeField('date published')
    truck_number = models.CharField(max_length=50)

class LicPlate(models.Model):
    plate_number = models.CharField(max_length=75)
    latitude = models.DecimalField(max_digits=12, decimal_places=7)
    longitude = models.DecimalField(max_digits=12, decimal_places=7)
    plate_image = models.FileField(upload_to='plate_uploads/plates/')
    overview_image = models.FileField(upload_to='plate_uploads/overviews/')
    entry_date = models.DateTimeField('date published')
    truck_number = models.CharField(max_length=50)

class customers(models.Model):
    email = models.CharField(max_length=75, unique=True)
    username = models.CharField(max_length=75, unique=True)
    password = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=75)
    entry_date = models.DateTimeField('date published')
    entryip = models.CharField(max_length=30)

'''
class hotlistManager(models.Manager):
    def create_hotlist(self, plate_number, customer_id, dateNow):
        #dateNow = datetime.now().date().__str__()
        hotlist = self.create(plate_number=plate_number, customer_id=customer_id, entry_date=dateNow)
        return hotlist
'''

class hotlist(models.Model):
    customer_id = models.ForeignKey(customers, on_delete=models.CASCADE)
    plate_number = models.CharField(max_length=30)
    entry_date = models.DateTimeField('date_published')
    entryip = models.CharField(max_length=30)




