from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.utils.crypto import get_random_string
from datetime import datetime, timedelta, time
from django.utils import timezone
from .models import Plate, LicPlate, customers, hotlist
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from decimal import *


import random
import re
import os

# Create your views here.
def index(request):

    if 'plate_keyword' not in request.POST:
        plate_keyword = ""
    else:
        plate_keyword = request.POST['plate_keyword']

    if 'multi_plates' not in request.POST:
        multi_plates = ""
    else:
        multi_plates = request.POST['multi_plates']

    if 'page' not in request.GET:
        page = 1
    else:
        page = request.GET['page']

    lic_plates = []
    if multi_plates != "":
        get_multi_plates = multi_plates.split('\n')
        multi_plates = "'" + ("', '").join(x.strip() for x in get_multi_plates) + "'"
        lic_plates = list(LicPlate.objects.raw('select * from plates_licplate WHERE plate_number in (' + multi_plates + ')'))
    elif plate_keyword != "":
        lic_plates = LicPlate.objects.filter(plate_number=plate_keyword)
        if lic_plates:
            for plate in lic_plates:
                customer_id = 1
                dateNow = timezone.now()
                c = customers.objects.get(pk=1)
                # check if that customer already entered plate into hotlist
                hotlist_items = hotlist.objects.filter(customer_id=c).filter(plate_number=plate_keyword)
                if not hotlist_items:
                    h = hotlist(plate_number=plate_keyword, customer_id=c, entry_date=dateNow)
                    h.save()
    else:
        lic_plates = LicPlate.objects.order_by('-entry_date')

    paginator = Paginator(lic_plates, 24)

    try:
        license_plates = paginator.page(page)
    except PageNotAnInteger:
        license_plates = paginator.page(1)
    except EmptyPage:
        license_plates = paginator.page(paginator.num_pages)
    context = {'lic_plates': license_plates}
    return render(request, 'plates/index.html', context)

def details(request, plate_id):
    return HttpResponse("This is the details page")

def account(request):

    customer_id = 1
    customer = customers.objects.get(pk=customer_id)

    return render(request, 'plates/account/index.html', {'customers': customer})

def save_account(request):
    customer_id = '1'
    customer = get_object_or_404(customers, pk=customer_id)

    if 'email' not in request.POST:
        email = ""
    else:
        email = request.POST['email']

    customer.email = email
    customer.save()

    return HttpResponseRedirect(reverse('plates:account'))



'''
def index(request):
    random.seed()
    for p in LicPlate.objects.all():
        plate_number = get_random_string(length=8)
        p.plate_number = plate_number.upper()
        p.truck_number = get_random_string(length=5)
        getcontext().prec = 7
        latitude = random.randint(3400000, 4200000)
        p.latitude = Decimal(latitude) / Decimal(100000)
        longitude = random.randint(8400000, 11500000)
        p.longitude = Decimal(longitude) / Decimal(100000)
        plate_image = os.path.basename(p.plate_image.name)
        plate_image = re.sub(r'\.[a-zA-Z0-9]{3,4}', r'.jpg', plate_image)
        p.plate_image = plate_image
        overview_image = os.path.basename(p.overview_image.name)
        overview_image = re.sub(r'\.[a-zA-Z0-9]{3,4}', r'.jpg', overview_image)
        p.overview_image = overview_image
        p.save()
'''