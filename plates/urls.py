from django.conf.urls import url

from . import views

app_name = 'plates'
urlpatterns = [

    url(r'^$', views.index, name='index'),

    # ex: /plates/5/details
    url(r'^(?P<plate_id>[0-9]+)/details/$', views.details, name='details'),

    # ex: /plates/account
    url(r'^account/$', views.account, name='account'),

    # ex: /plates/account
    url(r'^account/complete/$', views.save_account, name='save_account'),
]